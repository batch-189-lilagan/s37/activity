const Course = require('../models/Course.js');

// Create a new course
module.exports.addCourse = (reqBody) => {

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });

    return newCourse.save().then((course, error) => {

        if (error) {
            return false;
        }

        else {
            return true;
        }

    });

};

// Get all courses
module.exports.getAllCourses = async (data) => {

    if (data.isAdmin) {
        return Course.find({}).then(result => result);
    }

    else {
        return "Not an admin!"
    }
    

};

// Get all active courses
module.exports.getAllActive = () => {

    return Course.find({isActive:true}).then(result => result);

};

// Get specific course
module.exports.getCourse = (reqParams) => {

    return Course.findById(reqParams.courseId).then(result => result);

};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {

    let updatedCourse = {

        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price

    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

        if (error) {
            return false;
        }

        else {
            return true;
        }

    });

};

// Archiving a course by id and with admin privelage only
module.exports.archiveCourse = async (data, reqParams, reqBody) => {

    if (data.isAdmin) {
        let updatedStatus = {
            isActive: reqBody.isActive
        }
    
        return Course.findByIdAndUpdate(reqParams.courseId, updatedStatus).then((course, error) => {
    
            if (error) {
                return false
            }
    
            else {
                return true
            }
    
        })
    }

    else {
        return 'Not an admin!'
    }
    

}
