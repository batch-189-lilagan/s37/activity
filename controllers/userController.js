const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const Course = require('../models/Course.js');

// const ObjectId = require('mongodb').ObjectID;

// check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

    return User.find({email: reqBody.email}).then(result => {

        if (result.length > 0) {
            return true;
        }

        else {
            return false;
        }

    })

};

// Register a user
module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10), // (dataToBeHashed, saltRound)
        mobileNo: reqBody.mobileNo
    });

    return newUser.save().then((user, error) => {

        if (error) {
            return false;
        }

        else {
            return true;
        }

    })

};

// User authentication
module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then((result) => {

        if (result == null) {
            return false
        }

        else {
            
            // Will return bolean value (input/plaintext, hashed pass in db)
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                return { access: auth.createAccessToken(result) };
            }

            else {
                return false;
            }

        }

    });

};

// Find profile by Id
module.exports.getProfile = (data) => {

    return User.findById(data.id).then((result) => {

        if (result == null) {
            return false
        }

        else {

            result.password = '';

            return result;

        }

    })
    .catch(error => {
        return false;
    })

}

// Enroll a user to a class
module.exports.enroll = async (data) => {

    if (!data.isAdmin) {

        let isUserUpdated = await User.findById(data.userId).then(user => {

            user.enrollments.push({courseId: data.courseId});
    
            return user.save().then((user, error) => {
                if (error) {
                    return false
                }
    
                else {
                    return true
                }
            });
    
        });
    
        let isCourseUpdated = await Course.findById(data.courseId).then(course => {
    
            course.enrollees.push({userId: data.userId});
    
            return course.save().then((course, error) => {
                if (error) {
                    return false
                }
    
                else {
                    return true
                }
            })
    
        });
    
        // If udpating the user and course succeeds...
        if (isUserUpdated && isCourseUpdated) {
            return true;
        }
    
        else {
            return false;
        }
    }

    else {
        return 'Admin should not enroll a course'
    }

    

};


// When you findById using the input from postman or the user, it expects to receive an ObjectId data type. 
// When your input !== Object Id, it will return a casting error.
// If your input is !== Object Id, and its length is 12, it will proceed and treat it as an object Id
//  https://stackoverflow.com/questions/14940660/whats-mongoose-error-cast-to-objectid-failed-for-value-xxx-at-path-id
