const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController.js');
const auth = require('../auth.js');

// Route for checking if email exists in db
router.post("/checkEmail", (request, response) => {
    userController.checkEmailExists(request.body).then((resultFromController) => response.send(resultFromController));
});


// Route for registering a user
router.post("/register", (request, response) => {
    userController.registerUser(request.body).then((resultFromController) => response.send(resultFromController));
});


// Route for loggin in a user
router.post("/login", (request, response) => {
    userController.loginUser(request.body).then((resultFromController) => response.send(resultFromController));
});


// Route for getting user profile
router.get("/details", auth.verify, (request, response) => {

    const userData = auth.decode(request.headers.authorization);
    // const isAdminData = userData.isAdmin;
    // console.log(isAdminData);
    // console.log(request.headers.authorization);

    userController.getProfile({id: userData.id}).then((resultFromController) => response.send(resultFromController));
});


// Route for Enrolling an authenticated user
router.post('/enroll', auth.verify, (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        courseId: request.body.courseId,
    };


    userController.enroll(data).then(resultFromController => response.send(resultFromController));

})

module.exports = router;