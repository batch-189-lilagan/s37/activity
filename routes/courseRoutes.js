const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const courseController = require('../controllers/courseController.js');

// Route for creating a course
router.post("/", auth.verify, (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if (userData.isAdmin) {
        courseController.addCourse(request.body).then(resultFromController => response.send(resultFromController));
    }

    else {
        response.send('Error: Authorized admin only');
    }

});

// Route for retrieving all the courses
router.get('/all', auth.verify, (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    courseController.getAllCourses(userData).then(resultFromController => response.send(resultFromController));

});

// Route for retrieving all active courses
router.get('/', (request, response) => {

    courseController.getAllActive().then(resultFromController => response.send(resultFromController));

});

// Route for retrieving specific course
router.get('/:courseId', (request, response) => {

    console.log(request.params);

    courseController.getCourse(request.params).then(resultFromController => response.send(resultFromController));

});

// Route for updating a course by id
router.put('/:courseId', auth.verify, (request, response) => {

    courseController.updateCourse(request.params, request.body).then(resultFromController => response.send(resultFromController));

});


// Route for archiving a course by id 
router.put('/archive/:courseId', auth.verify, (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    courseController.archiveCourse(userData, request.params, request.body).then(resultFromController => response.send(resultFromController));

});

module.exports = router;