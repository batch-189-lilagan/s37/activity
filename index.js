const express = require('express');
const mongoose = require('mongoose');
// cors - allows our backend application to be availble to our frontend application
const cors = require('cors');
const userRoutes = require('./routes/userRoutes.js');
const courseRoutes = require('./routes/courseRoutes.js')
const port = 4000;

const app = express();

// Allows all resources to access our backend application
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// use the userRoute for this endpoint
app.use('/users', userRoutes);

// use the courseRoute for this endpoint
app.use('/courses', courseRoutes);


mongoose.connect('mongodb+srv://charleslilagandb:Mongodb09289955950@charleslilagan.chlqocw.mongodb.net/S37-S41?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDb Atlas! ♥'));




app.listen(process.env.PORT || port, () => console.log(`API is now online on port ${process.env.PORT || port}`));